﻿using game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endGame : MonoBehaviour
{
    bool started = false;
    [SerializeField] GameObject canvas;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() && !started)
        {
            started = true;
            StartCoroutine(WaitTillEnd());
        }
    }

    IEnumerator WaitTillEnd()
    {
        GetComponent<AudioSource>().Play();
        yield return new WaitWhile(() => GetComponent<AudioSource>().isPlaying);
            canvas.SetActive(true);
            canvas.GetComponent<Animator>().SetTrigger("end");
        yield return new WaitForSeconds(4);
            SceneManager.LoadScene("MainMenu");
    }

}
