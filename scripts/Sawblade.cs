﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace game
{
    public class Sawblade : MonoBehaviour
    {
        [SerializeField] GameObject sawblade;
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.GetComponent<Player>())
            {
                GetComponent<AudioSource>().Play();
                    sawblade.transform.position = new Vector3(15, -4.5f, 0);
                PlayerAccess.KillPlayer();
            }

        }
    }
}


