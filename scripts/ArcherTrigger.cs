﻿using game;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ArcherTrigger : MonoBehaviour
{
    [SerializeField] GameObject[] Archers;
    [SerializeField] TextMeshProUGUI textObject;
    [SerializeField] string text = "we ded RIP...";

    [SerializeField] GameObject arrow;

    bool alreadyShot = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Trigger>())
        {
            foreach (var item in Archers)
            {
                item.transform.rotation = Quaternion.Euler(0, 0, 72);
            }
                GetComponent<AudioSource>().Play();
            textObject.text = text;
        }
        if (collision.GetComponent<Player>() && !alreadyShot && !GameState.BoulderLaunched)
        {
            foreach (var item in Archers)
            {
                alreadyShot = true;
                GameObject spawnedArrow = Instantiate(arrow, item.transform.position, Quaternion.identity);

            }

        }
    }
}
