﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace game
{
    public class InteractionManager : MonoBehaviour
    {
        public GameObject InteractCanvas;

        [SerializeField] string[] introText = { "Hello there! For this game you need a good memory and you will die a lot!", 
                                                "Can you do that?" };
        [SerializeField] string introLoseText = "Well then I'll just finish you off right there and be done with it!";
        [SerializeField] string introCloseText = "Alright good luck in your adventure!";

        [SerializeField] TextMeshProUGUI text;
        [SerializeField] AudioClip cut;
        [SerializeField] AudioClip nooo;

        [SerializeField] GameObject button1;
        [SerializeField] GameObject button2;

        int currentTextIndex = 0;

        public bool canPlayerMove = false;

        bool lastText = false;
        bool introDone = false;
        bool readyToDie = false;

        // Start is called before the first frame update
        void Awake()
        {
            TextInteractions.SetUpObjects(InteractCanvas, text, button1, button2);
            TextInteractions.DoText(introText[currentTextIndex]);
            currentTextIndex++;
        }

        // Update is called once per frame
        void Update()
        {
            canPlayerMove = GameState.PlayerCanMove;
            if (!introDone)
            {
                DoIntro();
            }
        }

        #region Intro
        private void DoIntro()
        {
            if (Input.GetButtonDown("Fire1") && !PlayerAccess.PlayerDeath && GameState.GameRunning)
            {
                if (readyToDie)
                {
                    TextInteractions.CloseText();
                    PlayerAccess.KillPlayer();
                    GetComponent<AudioSource>().clip = cut;
                    GetComponent<AudioSource>().Play();
                }                
                else if (lastText)
                {
                    TextInteractions.CloseText();
                    introDone = true;
                    GameState.PlayerCanMove = true;
                }
                else if (introText.Length > currentTextIndex)
                {
                    TextInteractions.DoText(introText[currentTextIndex]);
                    currentTextIndex++;
                }
                else if (introText.Length <= currentTextIndex)
                {
                    TextInteractions.EnableButtons(WinIntro, LoseIntro);
                }

            }
        }

        void WinIntro()
        {
            TextInteractions.DoText(introCloseText);
            TextInteractions.DisableButtons();
            lastText = true;
        }

        void LoseIntro()
        {
            GetComponent<AudioSource>().clip = nooo;
            GetComponent<AudioSource>().Play();
            readyToDie = true;
            TextInteractions.DisableButtons();
            TextInteractions.DoText(introLoseText);
        }
        #endregion
    }
}

