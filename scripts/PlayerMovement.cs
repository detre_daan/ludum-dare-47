﻿using System.Collections;
using System.Collections.Generic;
using TMPro.Examples;
using UnityEngine;
using UnityEngine.Events;

namespace game
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] float moveSpeed = 5f;
        [SerializeField] float jumpForce = 5f;

        [SerializeField] float groundedRaycast = .1f;
        [SerializeField] LayerMask floorLayerMask;
        [SerializeField] Transform groundCheck;

        Rigidbody2D rb;

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            if (GameState.PlayerCanMove)
            {
                MovePlayer();
            }
        }

        private void MovePlayer()
        {
            var movement = Input.GetAxis("Horizontal");
            transform.position += new Vector3(movement, 0, 0) * Time.deltaTime * moveSpeed;

            if (!Mathf.Approximately(0, movement))
            {
                transform.rotation = movement > 0 ? Quaternion.Euler(0, 180, 0) : Quaternion.identity;
            }

            if (Input.GetButtonDown("Jump") && IsGrounded())
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode2D.Impulse);
            }
        }

        bool IsGrounded()
        {
            RaycastHit2D hit = Physics2D.Raycast(groundCheck.position, -Vector3.up, groundedRaycast, floorLayerMask);
            if (hit)
            {
                return true;
            }
            return false;
        }
    }
}

