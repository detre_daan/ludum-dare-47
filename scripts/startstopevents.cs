﻿using game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class startstopevents : MonoBehaviour
{
    [SerializeField] bool fullEnd = false;
    public void StartGame()
    {
        PlayerAccess.PlayerDeath = false;
        GameState.GameRunning = true;
        gameObject.SetActive(false);
    }

    public void StopGame()
    {
        GameState.GameRunning = false;
        if (!fullEnd)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        GameState.BoulderLaunched = false;
        GameState.CowsLanded = false;
    }
}
