﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DoSike : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GetComponent<AudioSource>().Play();
        text.text = "Sike";
    }
}
