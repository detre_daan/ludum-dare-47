﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace game
{
    public static class TextInteractions
    {
        public static void DoText(string text)
        {
            InteractionCanvas.SetActive(true);
            TextObject.text = text;
        }

        public static void CloseText()
        {
            InteractionCanvas.SetActive(false);
        }

        public static void SetUpObjects(GameObject interactionCanvas, TextMeshProUGUI textObject, GameObject button1, GameObject button2)
        {
            InteractionCanvas = interactionCanvas;
            TextObject = textObject;
            Button1 = button1;
            Button2 = button2;
        }
        public static void DisableButtons()
        {
            Button1.SetActive(false);
            Button2.SetActive(false);
        }

        public static void EnableButtons(Action method, Action method2)
        {
            Button1.SetActive(true);
            Button2.SetActive(true);

            Button1.GetComponent<Button>().onClick.AddListener(delegate { method(); });
            Button2.GetComponent<Button>().onClick.AddListener(delegate { method2(); });
        }

        public static GameObject InteractionCanvas { get; set; }
        public static TextMeshProUGUI TextObject { get; set; }
        public static GameObject Button1 { get; set; }
        public static GameObject Button2 { get; set; }
    }
}

