﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace game
{
    public class Player : MonoBehaviour
    {
        [SerializeField] GameObject gameOverObject;
        [SerializeField] Animator gameOverAnim;
        [SerializeField] GameObject particles;
        [SerializeField] GameObject spriteHolder;

        GameObject player;
        private void Start()
        {
            PlayerAccess.Player = gameObject;
            PlayerAccess.Particles = particles;
            PlayerAccess.GameOverAnim = gameOverAnim;
            PlayerAccess.SpriteHolder = spriteHolder;
            PlayerAccess.GameOverObject = gameOverObject;
        }
        public void KillHim()
        {
            Destroy(gameObject, 0.3f);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.GetComponent<Trigger>())
            {
                PlayerAccess.KillPlayer();
                GetComponent<AudioSource>().Play();
            }
        }
    }

    public static class PlayerAccess
    {
        public static GameObject Player { get; set; }
        public static GameObject Particles { get; set; }
        public static Animator GameOverAnim { get; set; }
        public static GameObject SpriteHolder { get; set; }
        public static GameObject GameOverObject { get; set; }

        public static bool PlayerDeath { get; set; } = false;

        public static void KillPlayer()
        {
            GameOverObject.SetActive(true);
            GameState.PlayerCanMove = false;
            GameOverAnim.SetTrigger("end");
            PlayerDeath = true;
            GameObject.Instantiate(Particles, Player.transform.position, Quaternion.identity);
            GameObject.Destroy(Player, 0.3f);
        }
    }
}

