﻿using game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boulder : MonoBehaviour
{
    [SerializeField] GameObject boulder;
    [SerializeField] float boulderForce = 5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!GameState.BoulderLaunched)
        {
            GetComponent<AudioSource>().Play();
        GameState.BoulderLaunched = true;
        boulder.GetComponent<Rigidbody2D>().AddForce(Vector3.left * boulderForce);
        }

    }
}
