﻿using game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cow : MonoBehaviour
{
    bool disabled = false;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() && !disabled && !PlayerAccess.PlayerDeath)
        {
            PlayerAccess.KillPlayer();
            Debug.Log("ded");
        }
        else if(!collision.gameObject.GetComponent<cow>()&& !collision.gameObject.GetComponent<Holder>())
        {
            GetComponent<AudioSource>().Play();
            disabled = true;
        }
    }
}
