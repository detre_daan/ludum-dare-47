﻿using game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrow : MonoBehaviour
{
    [SerializeField] float flySpeed = 5f;
    [SerializeField] AudioClip audio;
    GameObject player;

    bool reached = false;
    // Start is called before the first frame update
    void Start()
    {
        player = PlayerAccess.Player;
        GetComponent<AudioSource>().clip = audio;
        GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayerAccess.PlayerDeath)
        {
            if (!reached)
            {
                transform.position = Vector3.MoveTowards(transform.position, player.transform.position, flySpeed * Time.deltaTime);
            }
            if (Vector3.Distance(transform.position, player.transform.position) < 0.1f)
            {
                PlayerAccess.KillPlayer();
                transform.parent = player.transform;
                reached = true;
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject, 0.1f);
    }
}
