﻿using game;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace game
{
    public class comTrapTrigger : MonoBehaviour
    {
        [SerializeField] GameObject holder;
        [SerializeField] GameObject holder2;
        [SerializeField] TextMeshProUGUI text;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.GetComponent<Player>())
            {
                GetComponent<AudioSource>().Play();
                holder.SetActive(false);
                holder2.SetActive(false);
                text.gameObject.SetActive(true);
                StartCoroutine(DisableCows());
            }
        }

        IEnumerator DisableCows()
        {
            cow[] cows = FindObjectsOfType<cow>();
            yield return new WaitForSeconds(4);
            foreach (cow item in cows)
            {
                item.GetComponent<Collider2D>().enabled = false;
                item.GetComponent<Rigidbody2D>().isKinematic = true;
                item.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                GameState.CowsLanded = true;
            }
        }
    }
}

