﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace game
{
    public class LevelLoader : MonoBehaviour
    {

        // Start is called before the first frame update
        void Start()
        {
        }

        public void LoadNextScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        public void LoadStartScene()
        {
            SceneManager.LoadScene("MainMenu");
        }

        public void LoadLoseScene()
        {
            SceneManager.LoadScene("GameOver");
        }

        public void QuitGame()
        {
            Application.Quit();
        }


    }
}

