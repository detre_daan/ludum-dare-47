﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace game
{
    public static class GameState
    {
        public static bool PlayerCanMove { get; set; } = false;

        public static bool BoulderLaunched { get; set; } = false;

        public static bool GameRunning { get; set; } = false;

        public static bool CowsLanded { get; set; } = false;
    }
}
