﻿using game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevelLoad : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        GetComponent<AudioSource>().Play();
        StartCoroutine(WaitForJeej());

    }

    IEnumerator WaitForJeej()
    {
        yield return new WaitWhile(() => GetComponent<AudioSource>().isPlaying);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        GameState.PlayerCanMove = false;    
    }
}
