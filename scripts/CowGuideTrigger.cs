﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace game
{
    public class CowGuideTrigger : MonoBehaviour
    {
        [SerializeField] GameObject cow;
        [SerializeField] GameObject blocker;


        private void Update()
        {
            if (GameState.CowsLanded)
            {
                cow.SetActive(true);
                blocker.SetActive(false);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.GetComponent<Player>() && GameState.CowsLanded)
            {
                cow.GetComponent<Animator>().SetTrigger("guide");
            }
        }
    }
}

