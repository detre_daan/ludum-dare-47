﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace game
{
    public class Cave : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI trapText;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.GetComponent<Player>())
            {
                GetComponent<AudioSource>().Play();
                trapText.text = "SIKE!!";
                GameState.PlayerCanMove = false;
                PlayerAccess.SpriteHolder.SetActive(false);
                GetComponent<Animator>().SetTrigger("break");
            }
        }
        public void KillPlayer()
        {
            PlayerAccess.KillPlayer();
        }
    }
}

